package com.chanisata.swingtutorial;

import javax.swing.JButton;
import javax.swing.JFrame;

public class FirstSwingExem {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setTitle("First JFrame");
        JButton button = new JButton("click");
        button.setBounds(130, 100, 100, 40);
        frame.setLayout(null);
        frame.add(button);
        frame.setSize(400, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
