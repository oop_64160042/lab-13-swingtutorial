package com.chanisata.swingtutorial;

import javax.swing.JButton;
import javax.swing.JFrame;

public class ButtonExam1 extends JFrame {
    JButton button;

    public ButtonExam1() {
        super("Button Example");
        button = new JButton("Click here");
        button.setBounds(50, 100, 95, 30);
        this.add(button);
        this.setSize(400, 400);
        this.setLayout(null);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        ButtonExam1 frame = new ButtonExam1();

    }
}
